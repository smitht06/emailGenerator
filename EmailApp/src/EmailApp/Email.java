package EmailApp;

import java.util.Scanner;

public class Email {
    private String firstName;
    private String lastName;
    private String password;
    private int capacity = 500;
    private String email;
    private String department;
    private int passwordLength = 10;
    private String altEmail;
    private String companySite = "TurningPointGlobalSolutions.com";

    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = setDepartment();
        System.out.println("Department: " + this.department);
        this.password = randomPassword(passwordLength);
        System.out.println("your password is " + this.password);
        email = firstName.toLowerCase()+"." +lastName.toLowerCase() + "@" + department + "." +companySite;
    }
    private String setDepartment() {
        String departmentString;
        System.out.println("Enter The deparment:\n 1. sales \n 2. development\n 3. accounting \n 4. none");
        Scanner scan = new Scanner(System.in);
        int department = scan.nextInt();
        if(department == 1){
            departmentString = "Sales";
            return departmentString;
        }else if(department == 2){
            departmentString = "development";
            return departmentString;
        }else if(department == 3){
            departmentString = "Accounting";
            return departmentString;
        }else{
            return "Enter valid department";
        }

    }

    private String randomPassword(int length){
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYX1234567890!@#;";
        char[] password = new char[length];
        for(int i = 0; i <length; i++){
            int rand = (int)(Math.random()* passwordSet.length());
            password[i] = passwordSet.charAt(rand);
        }
        return new String(password);
    }
    public void setCapacity(int capacity){
        this.capacity = capacity;
    }
    public void setAltEmail(String altEmail){
        this.altEmail = altEmail;
    }
    public void changePassword(String password){
        this.password = password;
    }
    public int getCapacity(){
        return capacity;
    }
    public String getAltEmail(){
        return altEmail;
    }
    public String getPassword(){
        return password;
    }
    public String showInfo(){
        return  "Display name: "+ firstName + " " + lastName+"\n"+
                "Company Email:"+email+"\n"+
                "Mailbox Capacity: " +capacity + "mb";
    }

}
